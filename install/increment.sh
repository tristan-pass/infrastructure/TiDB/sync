#!/usr/bin/env bash
# 移动metadata 文件数据
mkdir /data/syncer
cp /data/mydumper/metadata /data/syncer/syncer.meta
vi /data/syncer/syncer.meta
# 修改格式:
# binlog-name = "mysql-bin.000003"
# binlog-pos = 930143241
# binlog-gtid = "2bfabd22-fff7-11e6-97f7-f02fa73bcb01:1-23,61ccbb5d-c82d-11e6-ac2e-487b6bd31bf7:1-4"
vi /data/syncer/config.toml
# 参考 config/config.toml 文件 或者 show master status; 的值


# 启动 syncer
cd /data/syncer
syncer -config /data/syncer/config.toml