#!/usr/bin/env bash
# 下载 tool 压缩包
wget http://download.pingcap.org/tidb-enterprise-tools-latest-linux-amd64.tar.gz
wget http://download.pingcap.org/tidb-enterprise-tools-latest-linux-amd64.sha256

# 检查文件完整性，返回 ok 则正确
sha256sum -c tidb-enterprise-tools-latest-linux-amd64.sha256
# 解开压缩包
tar -xzf tidb-enterprise-tools-latest-linux-amd64.tar.gz
ll tidb-enterprise-tools-latest-linux-amd64
ll tidb-enterprise-tools-latest-linux-amd64/bin
# 配置到环境变量
cp tidb-enterprise-tools-latest-linux-amd64/bin/* /usr/local/bin/