#!/usr/bin/env bash
# 导出上游mysql到文件
ls /data/mydumper/*
rm -rf /data/mydumper/*
mkdir -p /data/mydumper/

# 导出前注意
# # 查看是否开启了binlog
# show variables like '%log_bin%';
# # 查看是否有权限查看binlog
# show master status;

mydumper -v 3 -h *.*.*.* -P 3306 -u *** -p ****** -t 16 -F 64 -B yibai_ods --no-locks --skip-tz-utc -o /data/mydumper -T yibai_product_sku_ods
mydumper -v 3 -h *.*.*.* -P 3306 -u *** -p ****** -t 16 -F 64 -B yibai_ods --no-locks --skip-tz-utc -o /data/mydumper -T yibai_product_linelist
mydumper -v 3 -h *.*.*.* -P 3306 -u *** -p ****** -t 16 -F 64 -B yibai_ods --no-locks --skip-tz-utc -o /data/mydumper -T yibai_platform_listing_ods


# 导入前注意
# # sqlmode
# SELECT @@GLOBAL.sql_mode;
# SELECT @@SESSION.sql_mode;
# 对比上下游进行修改
# SET GLOBAL sql_mode = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION';
# SET SESSION sql_mode = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION';
# 通过查看 mysql.GLOBAL_VARIABLES.sql_mode的值 进行确认
# # batch_insert
#set @@tidb_batch_insert =ON;
#set @@tidb_batch_delete = ON;



# 导入上游mysql到tidb
myloader -v 3  -h 192.168.71.91 -u root -P 4000 -t 32 -q 1 -o -d /data/mydumper