# BI库

```
*.*.*.*:3306@***/******

数据库:
yibai_ods

表:
yibai_product_sku_ods			82w
yibai_product_linelist			0.25w
yibai_platform_listing_ods		2490w
```

创建文件夹

```
mkdir -p var/test
```

导出

```
ls ./var/test/*
rm -rf ./var/test/*
mkdir -p ./var/test/

mydumper -v 3 -h *.*.*.* -P 3306 -u *** -p ****** -t 16 -F 64 -B yibai_ods --no-locks --skip-tz-utc -o ./var/test
```

分表导出

```
ls var/test/*
rm -rf var/test/*
mkdir -p var/test/

mydumper -v 3 -h *.*.*.* -P 3306 -u *** -p ****** -t 16 -F 64 -B yibai_ods --no-locks --skip-tz-utc -o ./var/test -T yibai_product_sku_ods

mydumper -v 3 -h *.*.*.* -P 3306 -u *** -p ****** -t 16 -F 64 -B yibai_ods --no-locks --skip-tz-utc -o ./var/test -T yibai_product_linelist

mydumper -v 3 -h *.*.*.* -P 3306 -u *** -p ****** -t 16 -F 64 -B yibai_ods --no-locks --skip-tz-utc -o ./var/test -T yibai_platform_listing_ods
```

导入

```
myloader -v 3  -h 192.168.71.91 -u root -P 4000 -t 32 -d var/test
```



# 参考文档

<https://github.com/maxbube/mydumper/blob/master/docs/mydumper_usage.rst>

<https://github.com/maxbube/mydumper/blob/master/docs/myloader_usage.rst>